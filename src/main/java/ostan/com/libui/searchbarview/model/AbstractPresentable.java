package ostan.com.libui.searchbarview.model;



public abstract class AbstractPresentable<T>{

    protected final T presented;

    public AbstractPresentable(T object) {
        this.presented = object;
    }

    public abstract String getPresentableString();

    @Override
    public String toString() {
        return getPresentableString();
    }
}
