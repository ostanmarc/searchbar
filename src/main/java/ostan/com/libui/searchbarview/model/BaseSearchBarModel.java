package ostan.com.libui.searchbarview.model;

import android.arch.lifecycle.MutableLiveData;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import java.util.ArrayList;
import java.util.List;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;


public class BaseSearchBarModel<AbstractPresentable> extends BaseObservable {

    public MutableLiveData<String> queryString;
    public MutableLiveData<List<AbstractPresentable>> autoCompleteListObservable;
    public MutableLiveData<Boolean> isLoading;
    private IDataRepository<List<AbstractPresentable>> repository;
    public MutableLiveData<AbstractPresentable> selectedItem;

    private DisposableManager disposableManager;

    public BaseSearchBarModel(IDataRepository<List<AbstractPresentable>> repository) {

        this.repository = repository;

        queryString = new MutableLiveData<>();
        queryString.setValue("");

        autoCompleteListObservable = new MutableLiveData<>();
        autoCompleteListObservable.setValue(new ArrayList<>());

        isLoading = new MutableLiveData<>();
        isLoading.setValue(false);

        selectedItem = new MutableLiveData<>();

        disposableManager = new DisposableManager();

    }

    @Bindable
    public MutableLiveData<String> getQueryString() {
        return queryString;

    }

    public void setQueryString(String address) {
        if(this.queryString.getValue().equals(address)) {
            return;
        }
        this.queryString.setValue(address);
        if(!address.isEmpty()) {
            fetchAutoCompleteOptions(address);
        }
        notifyChange();
    }


    @Bindable
    public MutableLiveData<List<AbstractPresentable>> getAutoCompleteListObservable() {
        return autoCompleteListObservable;
    }

    @Bindable
    public MutableLiveData<Boolean> getIsLoading() {
        return isLoading;
    }

    private void setIsLoading(boolean isLoading){
        this.isLoading.setValue(isLoading);
        notifyChange();
    }

    private void fetchAutoCompleteOptions(String input) {
        this.isLoading.setValue(true);

        disposableManager.dispose();

        Single<List<AbstractPresentable>> fetchAction =
                repository.fetchData(input)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .doOnSuccess(abstractPresentables -> {
                            this.autoCompleteListObservable.setValue(abstractPresentables);
                            setIsLoading(false);
                        });

        disposableManager.add(fetchAction.subscribe());

    }

    public void setSelectedItem(AbstractPresentable item){
        selectedItem.setValue(item);
        notifyChange();
    }

    @Bindable
    public MutableLiveData<AbstractPresentable> getSelectedItem(){
        return selectedItem;
    }

}
