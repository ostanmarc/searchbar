package ostan.com.libui.searchbarview.model;

import io.reactivex.Single;

/**
 * Data fetcher interface.
 * Should implement call to the data repository
 * */
public interface IDataRepository<T> {
     Single<T> fetchData(String input);
}
