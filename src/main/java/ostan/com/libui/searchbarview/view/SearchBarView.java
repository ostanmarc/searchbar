package ostan.com.libui.searchbarview.view;

import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.LinearLayout;

import ostan.com.libui.databinding.SearchBarViewBinding;
import ostan.com.libui.searchbarview.viewmodel.SearchBarViewModel;


public class SearchBarView<T extends SearchBarViewModel> extends LinearLayout {

    SearchBarViewBinding binding;

    public SearchBarView(Context context) {
        super(context);
        init(context);
    }

    public SearchBarView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public SearchBarView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public SearchBarView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }


    private void init(Context context) {
        LayoutInflater inflater = LayoutInflater.from(context);
        binding = SearchBarViewBinding.inflate(inflater, this, true);
    }

    public void setModel(T model) {
        this.binding.setSearchViewModel(model);
    }

}
