package ostan.com.libui.searchbarview.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import ostan.com.libui.searchbarview.model.AbstractPresentable;

public class ArrayListPresentableAdapter<T>  extends ArrayAdapter<AbstractPresentable<T>> {

    int layoutResourse;
    List<AbstractPresentable<T>> presentablelist;

    public ArrayListPresentableAdapter(@NonNull Context context, int resource, @NonNull List<AbstractPresentable<T>> objects) {
        super(context, resource, objects);
        presentablelist = objects;
        this.layoutResourse = resource;

    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = View.inflate(getContext(), layoutResourse, null);
        }

        TextView tv = convertView.findViewById(android.R.id.text1);
        tv.setText(presentablelist.get(position).getPresentableString());
        convertView.setTag(presentablelist.get(position));
        return convertView;
    }




}
