package ostan.com.libui.searchbarview.utils;


import android.arch.lifecycle.LiveData;
import android.database.DataSetObserver;
import android.databinding.BindingAdapter;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;

import java.util.List;

import ostan.com.libui.searchbarview.model.AbstractPresentable;
import ostan.com.libui.searchbarview.view.ArrayListPresentableAdapter;
import ostan.com.libui.searchbarview.view.SearchBarView;
import ostan.com.libui.searchbarview.viewmodel.SearchBarViewModel;

public class SearchBarBindingAdapters {

    @BindingAdapter("android:search_bar_model")
    public static void setSearchModel(SearchBarView view, SearchBarViewModel searchBarModel) {
        view.setModel(searchBarModel);
    }

    @BindingAdapter("android:setIsLoading")
    public static void setLoading(View view, Boolean isLoading) {

        Log.i("TAG", "Loading in progress " + isLoading);

        view.setVisibility(isLoading ? View.VISIBLE : View.INVISIBLE);
    }

    @BindingAdapter("android:hints")
    public static void setHints(AutoCompleteTextView autoCompleteTextView,
                                List<AbstractPresentable> hints) {
        Log.i("TAG", "AutocompleteListArrived, num of hints: " + hints.size());
        ArrayListPresentableAdapter arrayAdapter = new ArrayListPresentableAdapter(
                autoCompleteTextView.getContext(), android.R.layout.select_dialog_item, hints);

        arrayAdapter.notifyDataSetChanged();
        autoCompleteTextView.setAdapter(arrayAdapter);
    }

    @BindingAdapter("android:itemSelectedListener")
    public static void setListener(AutoCompleteTextView autoCompleteTextView, AdapterView.OnItemClickListener listener) {
        autoCompleteTextView.setOnItemClickListener(listener);
    }

}
