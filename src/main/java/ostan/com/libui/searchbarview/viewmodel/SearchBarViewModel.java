package ostan.com.libui.searchbarview.viewmodel;

import android.arch.lifecycle.ViewModel;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;

import ostan.com.libui.searchbarview.model.AbstractPresentable;
import ostan.com.libui.searchbarview.model.BaseSearchBarModel;

public class SearchBarViewModel extends ViewModel {

    final public BaseSearchBarModel model;

    public SearchBarViewModel(BaseSearchBarModel model) {
        this.model = model;
    }

    public void setQuery(CharSequence address) {
        model.setQueryString(address.toString());
    }

    public final AdapterView.OnItemClickListener onItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
           Object tag = view.getTag();
           if(tag != null && tag instanceof AbstractPresentable) {
               model.setSelectedItem((AbstractPresentable) tag);
           } else {
               Log.i("TAG", "NO TAG OR WRONG TAG TYPE!");
           }
        }
    };

}
