## SearchBar repo

### This repository holds generic implementation of the Search Bar. 

Can be integrated in any View\Activity, as long as used with DataBinding.

## How to Use:

1. Clone the repo into your project
2. Add repo as module into your project
3. Add to your XML layour Ui View called <ostan.com.libui.searchbarview.view.SearchBarView>
 ** Important note ** 
 Your layout MUST must have data binding. 

Example:

<?xml version="1.0" encoding="utf-8"?>
<layout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools"
    xmlns:app="http://schemas.android.com/apk/res-auto">

    <data>

        <variable
            name="searchModel"
            type="ostan.com.libui.searchbarview.viewmodel.SearchBarViewModel" />

        <import type="ostan.com.libui.searchbarview.model.AbstractPresentable" />


    </data>

    <android.support.constraint.ConstraintLayout

        android:layout_width="match_parent"
        android:layout_height="match_parent"
        tools:context=".MainActivity">

        <TextView
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:text="Hello World!"
            app:layout_constraintBottom_toBottomOf="parent"
            app:layout_constraintLeft_toLeftOf="parent"
            app:layout_constraintRight_toRightOf="parent"
            app:layout_constraintTop_toTopOf="parent" />

        <ostan.com.libui.searchbarview.view.SearchBarView
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:search_bar_model="@{searchModel}">

        </ostan.com.libui.searchbarview.view.SearchBarView>

    </android.support.constraint.ConstraintLayout>

</layout>


Please obbserve here that you also should provide your SearchBar with a searchModel. 

4. Edit your MainActivity(assumed), in a way that will allow you to set the searchModel to the the binding:


 @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final ActivityMainBinding binding = ActivityMainBinding.inflate(LayoutInflater.from(this));

        setContentView(binding.getRoot());
        
        // State what type of Objects are you gonna be searching for. 
        // Provide the model with Repository class instance, which will perform the actual Search
        final BaseSearchBarModel<PresentablePojo> model = new BaseSearchBarModel<>(new PredictionsRepository());

        SearchBarViewModel searchViewModel = new SearchBarViewModel(model);

        binding.setSearchModel(searchViewModel);
        binding.setLifecycleOwner(this);

    }

5. Search model class will have @Bindable property called selected Item, you can observe to it and react to its changes in other parts of the app. 